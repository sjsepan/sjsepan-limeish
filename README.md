# Limeish Theme

Limeish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-limeish_code.png](./images/sjsepan-limeish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-limeish_codium.png](./images/sjsepan-limeish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-limeish_codedev.png](./images/sjsepan-limeish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-limeish_ads.png](./images/sjsepan-limeish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-limeish_theia.png](./images/sjsepan-limeish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-limeish_positron.png](./images/sjsepan-limeish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/11/2025
